package com.project.kevin.myfitnessplus3;

public class Meal {
    public String mealUID, mealName;
    public Double protein, fat, carb, cal;

    public Meal() {

    }

    public Meal( String mealName, Double protein, Double fat, Double carb, Double cal) {
       // this.mealUID = mealUID;
        this. mealName = mealName;
        this.protein = protein;
        this.fat = fat;
        this.carb = carb;
        this.cal = cal;
    }
}

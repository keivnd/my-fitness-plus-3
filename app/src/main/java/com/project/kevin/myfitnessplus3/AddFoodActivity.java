package com.project.kevin.myfitnessplus3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class AddFoodActivity extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private DatabaseReference userRef;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_food);

        // setup database references
        setupDatabaseReferences();

        Button btn_check_cal = findViewById(R.id.button_check_calories);
        Button btn_submit_food = findViewById(R.id.button_submit_food);

        btn_check_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView text_cal = findViewById(R.id.calories_food_add);

                // get form details
                EditText text_protein = findViewById(R.id.text_protein);
                EditText text_fat = findViewById(R.id.text_fat);
                EditText text_carb = findViewById(R.id.text_carb);
                Double protein = 0.0;
                Double fat = 0.0;
                Double carb = 0.0;

                protein = Double.parseDouble(text_protein.getText().toString());
                fat = Double.parseDouble(text_fat.getText().toString());
                carb = Double.parseDouble(text_carb.getText().toString());

                Double totalCal = protein * 4 + fat * 8 + carb * 4;

                text_cal.setText(totalCal.toString());

            }
        });

        btn_submit_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get form details
                EditText text_meal_name = findViewById(R.id.meal_name);

                EditText text_protein = findViewById(R.id.text_protein);
                EditText text_fat = findViewById(R.id.text_fat);
                EditText text_carb = findViewById(R.id.text_carb);
                Double protein = 0.0;
                Double fat = 0.0;
                Double carb = 0.0;

                String mealName = "";
                mealName = text_meal_name.getText().toString();

                protein = Double.parseDouble(text_protein.getText().toString());
                fat = Double.parseDouble(text_fat.getText().toString());
                carb = Double.parseDouble(text_carb.getText().toString());

                Double totalCal = protein * 4 + fat * 8 + carb * 4;

                // add food to database
                // get current date and use that as main key for this meal
                // also get a UID for meal
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
                String mealID = UUID.randomUUID().toString();
                DatabaseReference userMealRef = userRef.child(timeStamp+ "/" + mealID);
                userMealRef.child("meal-name").setValue(mealName);
                userMealRef.child("protein").setValue(protein);
                userMealRef.child("fat").setValue(fat);
                userMealRef.child("carb").setValue(carb);
                userMealRef.child("calories").setValue(totalCal);


                // finish
                startActivity(new Intent(AddFoodActivity.this, MainActivity.class));
            }
        });



    }

    private  void setupDatabaseReferences() {
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        // get firebase database instance
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference();
        //get current user
        user = FirebaseAuth.getInstance().getCurrentUser();
        userRef = rootRef.child("users/" + user.getUid());
    }
}

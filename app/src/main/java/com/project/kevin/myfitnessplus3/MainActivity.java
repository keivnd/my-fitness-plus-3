package com.project.kevin.myfitnessplus3;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private DatabaseReference userRef;
    private FirebaseUser user;
    private List<Meal> mealList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MealAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // setup database
        setupDatabaseReferences();



        // set goal calories to be displayed on screen
        DatabaseReference userProfileTDEE = userRef.child("user-profile").child("TDEE");
        userProfileTDEE.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Double TDEE = dataSnapshot.getValue(Double.class);
                TextView goalCal = findViewById(R.id.text_view_goal_cal);
                goalCal.setText(TDEE.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // display food
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mAdapter = new MealAdapter(mealList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        getTestMeals();





        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AddFoodActivity.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



    }

    private void getTestMeals() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        DatabaseReference ref = userRef.child("/" + timeStamp);
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot
                        getMealNutrition((Map<String,Object>) dataSnapshot.getValue());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }

    private void getMealNutrition(Map<String,Object> meals) {

        //iterate through each user, ignoring their UID
        for (Map.Entry<String, Object> entry : meals.entrySet()){

            //Get user map
            Map singleUser = (Map) entry.getValue();
            //Get phone field and append to list
            mealList.add(new Meal(singleUser.get("meal-name").toString(), Double.parseDouble(singleUser.get("protein").toString()), Double.parseDouble(singleUser.get("fat").toString()), Double.parseDouble(singleUser.get("carb").toString()), Double.parseDouble(singleUser.get("calories").toString())));
        }
        mAdapter.notifyDataSetChanged();
        //TODO: Calculate the food calories
        Double eatenFood = 0.0;
        for (Meal meal : mealList) {
            eatenFood += meal.cal;
        }
        TextView eaten_cal = findViewById(R.id.text_view_food);
        eaten_cal.setText(eatenFood.toString());

        // TODO: Calculate remaining calories
        TextView remaning_cal = findViewById(R.id.text_view_remaning_cal);
        TextView goal_cal = findViewById(R.id.text_view_goal_cal);

        Double goalCal = Double.parseDouble(goal_cal.getText().toString());
        Double eatenCal = Double.parseDouble(eaten_cal.getText().toString());
        Double remaningCal = Math.floor( goalCal - eatenCal);
        remaning_cal.setText(remaningCal.toString());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_user_profile) {
            startActivity(new Intent(MainActivity.this, ProfileDetails.class));
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(MainActivity.this, SettingActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private  void setupDatabaseReferences() {
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        // get firebase database instance
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference();
        //get current user
        user = FirebaseAuth.getInstance().getCurrentUser();
        userRef = rootRef.child("users/" + user.getUid());
    }

}

package com.project.kevin.myfitnessplus3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProfileDetails extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    private FirebaseDatabase database;
    private DatabaseReference rootRef;
    private DatabaseReference userRef;
    private FirebaseUser user;

    private Button btn_submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);
        // setup database references
        setupDatabaseReferences();

        Spinner spinner = findViewById(R.id.activity_level);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.activity_level, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        btn_submit = findViewById(R.id.button_submit_profile);
        // listen to submit button
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText text_weight = findViewById(R.id.weight);
                EditText text_height = findViewById(R.id.height);
                EditText text_age = findViewById(R.id.age);

                // Radio
                RadioGroup radio_gender = findViewById(R.id.gender);
                int selectedGenderId = radio_gender.getCheckedRadioButtonId();
                RadioButton selectedGender = findViewById(selectedGenderId);

                // Spinner
                Spinner spinner_activityLevel = findViewById(R.id.activity_level);

                //form values
                Float weight = Float.parseFloat(text_weight.getText().toString());
                Float height = Float.parseFloat(text_height.getText().toString());
                int age = Integer.parseInt(text_age.getText().toString());
                String gender = selectedGender.getText().toString();
                String selectedActvityLevel = spinner_activityLevel.getSelectedItem().toString();

                // calculate TDEE
                double TDEE = calculateTDEE(weight, height, age, gender, selectedActvityLevel);
                TDEE = Math.round(TDEE);
                // add values to database
                userRef = rootRef.child("users/" + user.getUid() + "/user-profile");
                /*userRef.child("weight").setValue(weight);
                userRef.child("height").setValue(height);
                userRef.child("age").setValue(age);
                userRef.child("gender").setValue(gender);
                userRef.child("activity_level").setValue(selectedActvityLevel);
                userRef.child("TDEE").setValue(TDEE);*/
                userRef.setValue(new UserProfile(weight, height, age, gender, selectedActvityLevel, TDEE));
                startActivity(new Intent(ProfileDetails.this, MainActivity.class));
            }
        });
    }

     private  void setupDatabaseReferences() {
        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        // get firebase database instance
        database = FirebaseDatabase.getInstance();
        rootRef = database.getReference();
        //get current user
        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    public Double calculateTDEE(float weight, float height, int age, String gender, String activityLevel) {
        double activityMult;
        switch (activityLevel) {
            case "Sedentary or light activity":  activityMult = 1.53;
                break;
            case "Active or moderately active":  activityMult = 1.76;
                break;
            case "Vigorously active":  activityMult = 2.25;
                break;
            default: activityMult = 0;
                break;
        }
        if(gender.equals("Male")) {
            return (10 * weight + 6.25 * height  - 5 * age + 5) * activityMult;
        } else {
            return (10 * weight + 6.25 * height  - 5 * age - 161) * activityMult;
        }

    }
}

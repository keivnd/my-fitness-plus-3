package com.project.kevin.myfitnessplus3;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MealAdapter  extends RecyclerView.Adapter<MealAdapter.MyViewHolder>{

    private List<Meal> mealList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mealTitle,  cal;

        public MyViewHolder(View view) {
            super(view);
            mealTitle = (TextView) view.findViewById(R.id.meal_title);
            cal = (TextView) view.findViewById(R.id.cal);
        }
    }
    public MealAdapter(List<Meal> mealList) {
        this.mealList = mealList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.meal_list_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Meal meal = mealList.get(position);
        holder.mealTitle.setText(meal.mealName);
        holder.cal.setText(meal.cal.toString() + " Calories");
    }

    @Override
    public int getItemCount() {
        return mealList.size();
    }
}

package com.project.kevin.myfitnessplus3;

public class UserProfile {
    public Float weight;
    public Float height;
    public int age;
    public String gender;
    public String selectedActvityLevel;
    public double TDEE;

    public UserProfile(Float weight, Float height, int age, String gender, String selectedActivityLevel, double TDEE) {
        this.weight = weight;
        this.height = height;
        this.age = age;
        this.gender = gender;
        this.selectedActvityLevel = selectedActivityLevel;
        this.TDEE = TDEE;
    }


}
